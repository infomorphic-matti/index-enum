#![doc = include_str!("../README.md")]

use proc_macro::TokenStream;

/// Derive macro for the IndexedEnum trait
#[proc_macro_derive(IndexedEnum, attributes(indexed_enum))]
pub fn make_indexed_enum_macro(input: TokenStream) -> TokenStream {
    TokenStream::new()
}

// index-enum - 0..N index-mappings for N-variant enums
// Copyright (C) 2024  Matti Bryce <mattibryce at protonmail dot com>
// 
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

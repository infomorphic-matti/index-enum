// This is the "top level" documentation.
#![doc = include_str!("../../README.md")]
#![cfg_attr(not(test), no_std)]

pub mod combinators;
pub mod marker;

use combinators::{IndexedEnumPrefixedWithUnit, IndexedEnumSum};
use core::fmt::Debug;
use either::Either;

#[cfg(feature = "derive")]
pub use index_enum_derive::IndexedEnum;

/// Value that can be used as an enum index.
pub trait EnumIndex: Copy + Debug + Eq + Ord + TryFrom<usize> + Sized {
    /// Zero value of this index type.
    const ZERO: Self;

    /// One-value of this index type.
    const ONE: Self;

    /// Maximum possible value of this type.
    const MAX: Self;

    /// Infallible conversion into usize.
    ///
    /// You should not implement this trait if this is not actually infallible (e.g. the pointer
    /// width is too small).
    fn infallible_into_usize(self) -> usize;
}

macro_rules! make_enum_index_for_nums {
    ($($num_ty:ty [$(
        // Conversion-to stuff.
        //
        // This should always include `usize` - the implementations are already conditional on the
        // pointer size too, so that is always handled.
        as $conv_num_ty:ty
    ),*] $(where $($ptr_widths:literal)*)? ),*) => {$(
        $(#[cfg(any($(target_pointer_width = $ptr_widths),*))])?
        impl EnumIndex for $num_ty {
            const ZERO: $num_ty = 0;
            const ONE: $num_ty = 1;
            const MAX: $num_ty = <$num_ty>::MAX;

            #[inline(always)]
            fn infallible_into_usize(self) -> usize {
                self as usize
            }
        }

        $(#[cfg(any($(target_pointer_width = $ptr_widths),*))])?
        impl <
            T1: IndexedEnum<Index = $num_ty>,
            T2: IndexedEnum<Index = $num_ty>
        > combinators::IndexedEnumSum for ((T1, $num_ty), (T2, $num_ty)) {
            type Unified = $num_ty;

            type T1 = T1;
            type T2 = T2;

            // If this overflows, it will error at compile time, and this is the max value any variant
            // can produce.
            //
            // So the only way something overflowing can occur is if the user provides an out of
            // range index compared to this, which is already warned against.
            const TOTAL_SIZE: Self::Unified = <T1 as IndexedEnum>::SIZE + <T2 as IndexedEnum>::SIZE;

            #[inline(always)]
            fn unify_first(v: $num_ty) -> $num_ty {
                v
            }

            #[inline(always)]
            fn unify_second(v: $num_ty) -> $num_ty {
                <T1 as IndexedEnum>::SIZE + v
            }

            #[inline]
            fn bifurcate_index(v: $num_ty) -> Option<Either<$num_ty, $num_ty>> {
                match v {
                    v if (0..<T1 as IndexedEnum>::SIZE).contains(&v) => Some(Either::Left(v)),
                    // "associated consts can't be referenced in patterns"
                    // otherwise we'd do a <T1 as IndexedEnum>::SIZE.. pattern
                    v => Some(Either::Right(v - <T1 as IndexedEnum>::SIZE))
                }
            }
        }

        // This is up-conversions when we are the first index space and the to-be-converted type is
        // the next index space.
        make_enum_index_for_nums!{
            @make_upconvs_for $num_ty
                [$(as $conv_num_ty),*]
                [$(#[cfg(any($(target_pointer_width = $ptr_widths),*))])?]
        }
    )*};
    // Avoid "repeating at level" errors.
    //
    // This is the deepest config and directly produces the impl. It requires that the target
    // numeric type is of larger width than the source, and takes raw meta args that have been
    // bundled up into a tt to traverse all the other repeating args.
    //
    // This does both the case where the current numeric type is the index of the first indexed
    // enum, as well as the case where the current numeric type is the index of the second indexed
    // enum.
    (@upconv_conditional_cfg [$(#[$meta:meta])*] $num_ty:ty as $conv_num_ty:ty) => {
        // We are first.
        $(#[$meta])*
        impl <
            T1: IndexedEnum<Index = $num_ty>,
            T2: IndexedEnum<Index = $conv_num_ty>
        > combinators::IndexedEnumSum for ((T1, $num_ty), (T2, $conv_num_ty)) {
            // Conversion is upwards.
            type Unified = $conv_num_ty;

            type T1 = T1;
            type T2 = T2;

            const TOTAL_SIZE: Self::Unified = (<T1 as IndexedEnum>::SIZE as $conv_num_ty) + <T2 as IndexedEnum>::SIZE;

            #[inline(always)]
            fn unify_first(v: $num_ty) -> Self::Unified {
                v as $conv_num_ty
            }

            #[inline(always)]
            fn unify_second(v: $conv_num_ty) -> Self::Unified {
                (<T1 as IndexedEnum>::SIZE as $conv_num_ty) + v
            }

            #[inline]
            fn bifurcate_index(idx: $conv_num_ty) -> Option<Either<$num_ty, $conv_num_ty>> {
                match idx {
                   idx if (0..<T1 as IndexedEnum>::SIZE as $conv_num_ty).contains(&idx) => Some(Either::Left(idx.try_into().ok()?)),
                   idx => Some(Either::Right(idx - <T1 as IndexedEnum>::SIZE as $conv_num_ty))
                }
            }
        }

        // We (the "smaller" number) are second
        $(#[$meta])*
        impl <
            T1: IndexedEnum<Index = $conv_num_ty>,
            T2: IndexedEnum<Index = $num_ty>
        > combinators::IndexedEnumSum for ((T1, $conv_num_ty), (T2, $num_ty)) {
            // Conversion is upwards
            type Unified = $conv_num_ty;

            type T1 = T1;
            type T2 = T2;

            const TOTAL_SIZE: Self::Unified = <T1 as IndexedEnum>::SIZE + (<T2 as IndexedEnum>::SIZE as $conv_num_ty);


            #[inline(always)]
            fn unify_first(v: $conv_num_ty) -> Self::Unified {
                v
            }

            #[inline(always)]
            fn unify_second(v: $num_ty) -> Self::Unified {
                <T1 as IndexedEnum>::SIZE  + (v as $conv_num_ty)
            }


            #[inline]
            fn bifurcate_index(idx: $conv_num_ty) -> Option<Either<$conv_num_ty, $num_ty>> {
                match idx {
                   idx if (0..<T1 as IndexedEnum>::SIZE).contains(&idx) => Some(Either::Left(idx)),
                   idx => Some(Either::Right((idx - <T1 as IndexedEnum>::SIZE).try_into().ok()?))
                }
            }
        }
    };
    (@make_upconvs_for $num_ty:ty [$(as $conv_num_ty:ty),*] $meta_bundle:tt) => {$(
        make_enum_index_for_nums!{@upconv_conditional_cfg $meta_bundle $num_ty as $conv_num_ty}
    )*}
}

// Constrain on pointer widths
make_enum_index_for_nums! {
    u8 [as u16, as u32, as u64, as usize],
    u16 [as u32, as u64, as usize] where "16" "32" "64",
    u32 [as u64, as usize] where "32" "64",
    u64 [as usize] where "64",
    usize []
}

/// Enum that has a full mapping between the range [0, N) (where N is it's variant count), and it's
/// variants of an enum. Every number in the integer set {0, 1, ..., N-1} must be mapped to exactly
/// one variant, and vice versa. The mapping does not have to be in order of variant declaration,
/// but it *must* be bidirectional (index -> variant is symmetrical to variant -> index).  
///
/// Enums can configure their own index type, but it must be "number-like" - in particular, it must
/// implement [`EnumIndex`]. Invariants of manually implemented indices can be validated by the
/// functon [`validate_indexed_enum_implementation`] available with feature `test`, though they
/// can't be *fully* validated.
///
/// Something important to note. The index of an enum variant is completely different to any
/// provided enum discriminants.
pub trait IndexedEnum: Sized {
    /// Index of the enum.
    type Index: EnumIndex;

    /// Number of variants in the enum.
    ///
    /// The mapping of indices to variants must encompass all values in the range
    /// [`EnumIndex::ZERO`] up to (but not including) [`IndexedEnum::SIZE`].
    const SIZE: Self::Index;

    /// Get the enum variant associated with the index.
    ///
    /// Must return [`Some`] when the index is inside the range [`EnumIndex::ZERO`] (for
    /// [`IndexedEnum::Index`]) to [`IndexedEnum::SIZE`] (but the size is not included in the
    /// range).
    ///
    /// It may return [`None`] when outside the range, but it's not required to. People
    /// using this trait should validate the range inputs before putting it into this function.
    /// This is important, as it allows for significant efficiency gains in certain trivial cases.
    fn from_index(index: Self::Index) -> Option<Self>;

    /// Get the index for the enum variant in question.
    fn get_index(&self) -> Self::Index;
}

#[cfg(feature = "test")]
/// Validate your manual implementation of [`IndexedEnum`], to ensure that the functions in
/// question are correct. This will panic on failure, and can be disabled by turning off the
/// `test` feature.
///
/// This will ensure that all the indices inside your enum's implementation convert to variants
/// that are bidirectional. However, it cannot validate the following facts:
/// * Enum variants with non-zero-sized data have a full bidirectional to the index space
///   over all possible values of the data. An enum variant could pathologically hide information
///   on an entire subset of indices and regenerate the index from itself.
/// * Enums that mutate globally-synchronised state have pure `from_index` and `get_index`
///   functions that don't change in value between calls. This is another pathological case.
/// * All the variants of your enum are covered by the trait.  
#[allow(unused_imports)]
pub fn validate_indexed_enum_implementation<T: IndexedEnum>()
where
    core::ops::Range<<T as IndexedEnum>::Index>: IntoIterator<Item = <T as IndexedEnum>::Index>,
{
    // This loop asserts *perfect bidirectionality* within the relevant domain. Because we cycle
    // over every input index, and retrieve the result of a full bidirectional cycle, two variants
    // with the same output will be detected the moment the index that does not generate that
    // variant when provided as input is tested.
    for index in <T as IndexedEnum>::Index::ZERO..<T as IndexedEnum>::SIZE {
        let maybe_enum_value = T::from_index(index);
        let enum_value = match maybe_enum_value {
            Some(v) => v,
            None => panic!("Index in the range of indices indicated by an indexed enum did not have an associated variant: {:?}", index), 
        };
        let reverse_index = enum_value.get_index();
        assert_eq!(reverse_index, index);
    }
}

// Some fairly core implementations of [`IndexedEnum`] that map on to more standard types.
impl IndexedEnum for () {
    type Index = u8;

    const SIZE: Self::Index = 1;

    #[inline]
    fn from_index(_index: Self::Index) -> Option<Self> {
        Some(())
    }

    #[inline]
    fn get_index(&self) -> Self::Index {
        0
    }
}

impl IndexedEnum for core::marker::PhantomPinned {
    type Index = u8;

    const SIZE: Self::Index = 1;

    #[inline]
    fn from_index(_index: Self::Index) -> Option<Self> {
        Some(Self)
    }

    #[inline]
    fn get_index(&self) -> Self::Index {
        0
    }
}

impl<T: ?Sized> IndexedEnum for core::marker::PhantomData<T> {
    type Index = u8;

    const SIZE: Self::Index = 1;

    #[inline]
    fn from_index(_index: Self::Index) -> Option<Self> {
        Some(Self)
    }

    #[inline]
    fn get_index(&self) -> Self::Index {
        0
    }
}

impl IndexedEnum for bool {
    type Index = u8;

    const SIZE: Self::Index = 2;

    #[inline]
    fn from_index(index: Self::Index) -> Option<Self> {
        Some(index > 0)
    }

    #[inline]
    fn get_index(&self) -> Self::Index {
        match self {
            false => 0,
            true => 1,
        }
    }
}

impl<T: IndexedEnumPrefixedWithUnit> IndexedEnum for Option<T>
where
    combinators::EnumIndexUnitPrefixSum<T>: IndexedEnumSum,
{
    type Index = T::PrefixedIndex;

    const SIZE: Self::Index = T::WITH_INDEX_PREFIX_SIZE;

    #[inline]
    fn from_index(index: Self::Index) -> Option<Self> {
        // The weird `Self::` thing should help improve the clarity of the nested optionals.
        match <T as IndexedEnumPrefixedWithUnit>::classify_index(index) {
            Some(Either::Left(())) => Some(Self::None),
            Some(Either::Right(base_index)) => T::from_index(base_index).map(Self::Some),
            None => None,
        }
    }

    #[inline]
    fn get_index(&self) -> Self::Index {
        self.as_ref()
            .map(T::get_index)
            .map(T::shift_normal_index)
            .unwrap_or(T::prefix_index())
    }
}

#[cfg(all(test, feature = "test"))]
mod test {
    use core::marker::{PhantomData, PhantomPinned};

    use super::{validate_indexed_enum_implementation as validate, IndexedEnum};

    #[test]
    fn units() {
        validate::<()>();
        validate::<PhantomPinned>();
        validate::<PhantomData<dyn Iterator<Item = usize>>>();
    }

    #[test]
    fn boolean() {
        validate::<bool>();
    }

    #[test]
    fn optional() {
        validate::<Option<bool>>();
        assert_eq!(<Option<bool> as IndexedEnum>::SIZE, 3);
        assert_eq!(Option::<bool>::from_index(0).unwrap(), None);
        assert_eq!(Option::<bool>::from_index(1).unwrap(), Some(false));
        assert_eq!(Option::<bool>::from_index(2).unwrap(), Some(true));
    }

    #[test]
    #[should_panic]
    /// Test for detecting implementations where more than one index maps to the same variant.
    fn bad_detection_duplicates() {
        /// Enum that contains a duplicate index.
        #[derive(Debug, Clone, Copy)]
        enum ContainsDuplicate {
            First,
            Second,
            Third,
        }

        /// This is a BAD implementation. DO NOT COPY IT.
        impl IndexedEnum for ContainsDuplicate {
            type Index = u8;

            const SIZE: Self::Index = 4;

            fn from_index(index: Self::Index) -> Option<Self> {
                match index {
                    0 => Some(Self::First),
                    1 => Some(Self::Second),
                    // Note the duplicate here - this is not valid.
                    2 => Some(Self::First),
                    3 => Some(Self::Third),
                    _ => None,
                }
            }

            fn get_index(&self) -> Self::Index {
                match self {
                    ContainsDuplicate::First => 0,
                    ContainsDuplicate::Second => 1,
                    // Skipping 2 because that's where the duplicate is
                    ContainsDuplicate::Third => 3,
                }
            }
        }

        validate::<ContainsDuplicate>();
    }

    #[test]
    #[should_panic]
    /// Test the detection of cases where the index -> enum map is not the bidirectional inverse of
    /// enum -> index.
    fn bad_detection_map_asymmetry() {
        #[derive(Debug, Copy, Clone)]
        pub enum NonBidirectionalMapping {
            First,
            Second,
            Third,
            Fourth,
            Fifth,
        }

        /// This is a BAD implementation. DO NOT COPY IT.
        impl IndexedEnum for NonBidirectionalMapping {
            type Index = u8;

            const SIZE: Self::Index = 5;

            fn from_index(index: Self::Index) -> Option<Self> {
                match index {
                    0 => Some(Self::Third),
                    1 => Some(Self::Second),
                    2 => Some(Self::First),
                    3 => Some(Self::Fourth),
                    4 => Some(Self::Fifth),
                    _ => None,
                }
            }

            fn get_index(&self) -> Self::Index {
                match self {
                    NonBidirectionalMapping::First => 2,
                    NonBidirectionalMapping::Second => 1,
                    NonBidirectionalMapping::Third => 0,
                    // This bit down here is where the error is - the indices are swapped from
                    // their `from_index` result.
                    NonBidirectionalMapping::Fourth => 4,
                    NonBidirectionalMapping::Fifth => 3,
                }
            }
        }
        validate::<NonBidirectionalMapping>();
    }

    #[test]
    #[should_panic]
    /// Test that validation successfully identifies range holes.
    fn map_has_hole() {
        #[derive(Debug, Clone, Copy)]
        // Second variant is never constructed.
        //
        // In this case that would actually act as a good warning sign, but in reality such a state
        // is not likely to be maintained in a real codebase.
        #[allow(dead_code)]
        pub enum MapHasHole {
            First,
            Second,
            Third,
        }

        /// This is a BAD implementation. DO NOT COPY IT.
        impl IndexedEnum for MapHasHole {
            type Index = u8;

            const SIZE: Self::Index = 3;

            fn from_index(index: Self::Index) -> Option<Self> {
                match index {
                    0 => Some(Self::First),
                    // This is where the error is. The range [0, 3) must be isomorphic with the
                    // enum.
                    1 => None,
                    2 => Some(Self::Third),
                    _ => None,
                }
            }

            fn get_index(&self) -> Self::Index {
                match self {
                    MapHasHole::First => 0,
                    MapHasHole::Second => 1,
                    MapHasHole::Third => 2,
                }
            }
        }

        validate::<MapHasHole>();
    }
}

// index-enum - 0..N index-mappings for N-variant enums
// Copyright (C) 2024  Matti Bryce <mattibryce at protonmail dot com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

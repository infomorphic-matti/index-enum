//! Utilities that allow combining indexed enums.
use either::Either;

use crate::{marker::PhantomUnitIndexed, EnumIndex, IndexedEnum};

mod indexed_enum_sum_combinator_seal {
    use crate::IndexedEnum;

    /// Sealed trait that is only implemented on pairs of indexed enum tuples.
    pub trait Seal {}

    impl<T1: IndexedEnum, T2: IndexedEnum> Seal for ((T1, T1::Index), (T2, T2::Index)) {}
}

/// [Either] the [`EnumIndex`] of `T1` (an [`IndexedEnum`]), or the [`EnumIndex`] of `T2` (also an
/// [`IndexedEnum`])
pub type EitherIndexOf<T1, T2> = Either<<T1 as IndexedEnum>::Index, <T2 as IndexedEnum>::Index>;

/// Composed pair of indexed enums. This trait is used to enable some powerful combinators of enum
/// indices and by-proxy, of [`IndexedEnum`]. This is an ordered combinator - T1 has the first part
/// of the index space, and then T2 has the next part of the index space. The combined index space
/// should start at zero and go up to [`IndexedEnumSum::TOTAL_SIZE`] (not including the size
/// itself).
///
/// This should be implemented on tuples with `((T1, T1::Index), (T2, T2::Index))`. Being generic
/// over the Indexed Enum part (T1 and T2) is good, then this trait allows you to control how to
/// combine specific types of enum indices, which allows for constant stuff and avoids issues of
/// collisions when doing blanket impls.
///
/// Essentially, you want to do something like:
/// ```rust,ignore
/// impl <
///     T1: IndexedEnum<Index = YourMagicIndexType>,
///     T2: IndexedEnum<Index = u16>
/// > IndexedEnumSum for ((T1, YourMagicIndexType), (T2, u16))
/// ```
///
/// Then, users composing indexed enums will be able to use one implementation for the indexed enum
/// trait that combines things, by placing a requirement that looks like:
///  `where ((T1, T1::Index), (T2, T2::Index)): IndexedEnumSum<T1 = T1, T2 = T2>`
///
///  (note: The constraints on the associated types are in fact necessary, as otherwise Rust cannot
///  prove that the `T1`/`T2` in every impl of [`IndexedEnumSum`] are the same as those in the
///  tuple itself (as composed by the user).
///
/// It should be noted that this weirdness is at least partially:
/// * A mitigation for lack of more intense generic constants
/// * A mitigation for the [trivial_bounds issue][trivial-bounds]
/// * A mitigation for the issue where the current trait constraint solver can't identify distinct
///   associated type constraints as indicating a disjoint implementation - see [this
///   stackoverflow][disjoint-trait-impls]
///
/// [trivial-bounds]: https://github.com/rust-lang/rust/issues/48214#issuecomment-1374378038,
/// [disjoint-trait-impls]: https://stackoverflow.com/questions/40392524/conflicting-trait-implementations-even-though-associated-types-differ/40408431#40408431
pub trait IndexedEnumSum: indexed_enum_sum_combinator_seal::Seal {
    /// New, unified enum index type to use.
    type Unified: EnumIndex;

    // Note - we initially added some extra `T1Index` and `T2Index` and added them as constraints
    // to these types. But that doesn't work well with type deduction.
    type T1: IndexedEnum;
    type T2: IndexedEnum;

    /// Size of the unified enum index, T1 *then* T2, should be basically `T1::SIZE` + `T2::SIZE`
    const TOTAL_SIZE: Self::Unified;

    /// Unify an index from the first chunk of the index space, into unified form.
    fn unify_first(v: <Self::T1 as IndexedEnum>::Index) -> Self::Unified;

    /// Unify an index from the second chunk of the index space, into unified form.
    ///
    /// This should shift up to the translated form of `T1` index space so that
    /// the [`EnumIndex::ZERO`] of T2's index space is translated to where `T1::SIZE` would be when
    /// unified.
    fn unify_second(v: <Self::T2 as IndexedEnum>::Index) -> Self::Unified;

    /// Split a unified index, deciding which indexed enum it comes from (or if it is an invalid
    /// index).
    ///
    /// Just like in [`IndexedEnum`], this does not have to produce [`None`] if the input value is
    /// outside of the formal range of the unified index space. It *may* do so, however.
    fn bifurcate_index(v: Self::Unified) -> Option<EitherIndexOf<Self::T1, Self::T2>>;
}

/// Shorthand for the [`IndexedEnumSum`] type used when creating an index space prefix for
/// [`IndexedEnumPrefixedWithUnit`] and placing where bounds on it.
///
/// This is mostly useful because sometimes you will need to supply a bound on [`IndexedEnumSum`]
/// to use [`IndexedEnumPrefixedWithUnit`], and the manual type will give you carpal tunnel
/// syndrome to type out constantly :p. For notes on correctly bounding on [`IndexedEnumSum`] when
/// building combinators, see that trait's documentation. 
pub type EnumIndexUnitPrefixSum<S> = (
    (
        PhantomUnitIndexed<<S as IndexedEnum>::Index>,
        <S as IndexedEnum>::Index,
    ),
    (S, <S as IndexedEnum>::Index),
);

/// The [`IndexedEnum`] will implement this trait if it is compatible with a mode where one more
/// unit enum variant/index can be "pushed" before it's own index space using the zero-value in
/// the index space and shuffling everything else along.
///
/// This is automatically implemented when your index types provide generic implementations of
/// [`IndexedEnumSum`].
pub trait IndexedEnumPrefixedWithUnit: IndexedEnum
where
    EnumIndexUnitPrefixSum<Self>: IndexedEnumSum,
{
    /// Index type that can include the prefix.
    type PrefixedIndex: EnumIndex;

    /// The size of the enum index space when the prefix is included.
    const WITH_INDEX_PREFIX_SIZE: Self::PrefixedIndex;

    /// Index that represents the extra component of the index space at the start.
    fn prefix_index() -> Self::PrefixedIndex;

    /// Convert an index of the base [`IndexedEnum`] into one shifted to account for the prefix
    fn shift_normal_index(idx: Self::Index) -> Self::PrefixedIndex;

    /// Turn a "shifted" index into either the prefix space (`()`), or an index into the base enum,
    /// or of course a failure if it's not valid.
    fn classify_index(idx: Self::PrefixedIndex) -> Option<Either<(), Self::Index>>;
}

impl<T: IndexedEnum> IndexedEnumPrefixedWithUnit for T
where
    EnumIndexUnitPrefixSum<T>: IndexedEnumSum<T1 = PhantomUnitIndexed<T::Index>, T2 = T>,
{
    type PrefixedIndex = <EnumIndexUnitPrefixSum<T> as IndexedEnumSum>::Unified;

    const WITH_INDEX_PREFIX_SIZE: Self::PrefixedIndex =
        <EnumIndexUnitPrefixSum<Self> as IndexedEnumSum>::TOTAL_SIZE;

    #[inline(always)]
    fn prefix_index() -> Self::PrefixedIndex {
        EnumIndexUnitPrefixSum::<Self>::unify_first(<Self::Index as EnumIndex>::ZERO)
    }

    #[inline(always)]
    fn shift_normal_index(idx: T::Index) -> Self::PrefixedIndex {
        <EnumIndexUnitPrefixSum<Self> as IndexedEnumSum>::unify_second(idx)
    }

    #[inline]
    fn classify_index(idx: Self::PrefixedIndex) -> Option<Either<(), Self::Index>> {
        match EnumIndexUnitPrefixSum::<Self>::bifurcate_index(idx) {
            Some(Either::Left(_)) => Some(Either::Left(())),
            Some(Either::Right(v)) => Some(Either::Right(v)),
            None => None,
        }
    }
}

// index-enum - 0..N index-mappings for N-variant enums
// Copyright (C) 2024  Matti Bryce <mattibryce at protonmail dot com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

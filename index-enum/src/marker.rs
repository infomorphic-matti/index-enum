//! Contains marker types.

use core::marker::PhantomData;

use crate::{EnumIndex, IndexedEnum};

/// Phantom [`IndexedEnum`] which has a single unit variant, and is indexed by the [`EnumIndex`]
/// provided as a type parameter, with exactly one value of zero and a size of exactly one. It is
/// comparable to the implementation for the actual unit type.
#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Default, Hash)]
pub struct PhantomUnitIndexed<I>(pub PhantomData<I>);

impl<I: EnumIndex> IndexedEnum for PhantomUnitIndexed<I> {
    type Index = I;

    const SIZE: Self::Index = I::ONE;

    #[inline(always)]
    fn from_index(_: Self::Index) -> Option<Self> {
        Some(Self(PhantomData))
    }

    #[inline(always)]
    fn get_index(&self) -> Self::Index {
        I::ZERO
    }
}

impl<I> From<PhantomUnitIndexed<I>> for () {
    #[inline(always)]
    fn from(_: PhantomUnitIndexed<I>) {}
}

impl<I> From<()> for PhantomUnitIndexed<I> {
    #[inline(always)]
    fn from(_: ()) -> Self {
        Self(PhantomData)
    }
}

// index-enum - 0..N index-mappings for N-variant enums
// Copyright (C) 2024  Matti Bryce <mattibryce at protonmail dot com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

# index-enum
Map 0..N indices to enums with N variants, bidirectionally - with customisable out-of-order mapping - using the [`IndexedEnum`] trait. 

Enums can be indexed by anything implementing the [`EnumIndex`] trait, which entails infallible convertibility to [`usize`] (and fallible convertibility from [`usize`]).

By providing `features = ["derive"]` in `Cargo.toml` allows you to automatically derive the [`IndexedEnum`]. This is the preferred method to implement the trait, as it provides means to mitigate any correctness issues and avoid accidentally doing things that could break the bidirectional mapping between the index space and the space of enum variants.
